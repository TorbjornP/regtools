// Convergence testing of nlinregr using datasets from nistdataset() ...except the Nelson.dat which is not correctly read in from nistdata_read().
// Torbjørn Pettersen, July 2012. 

warning('off');
sets=['Chwirut1.dat','nls','lower','exp(-b1*x)./(b2+b3*x)','b1 b2 b3','[-x.*exp(-b1*x)./(b3*x+b2),-exp(-b1*x)./(b3*x+b2).^2,-x.*exp(-b1*x)./(b3*x+b2).^2]';
     'DanWood.dat','nls','lower','b1*x.^b2','b1 b2','';
     'Gauss1.dat','nls','lower','b1*exp( -b2*x ) + b3*exp( -(x-b4).^2 / b5.^2 ) + b6*exp( -(x-b7).^2 / b8.^2 )','b1 b2 b3 b4 b5 b6 b7 b8','';
     'Gauss2.dat','nls','lower','b1*exp( -b2*x ) + b3*exp( -(x-b4).^2 / b5.^2 ) + b6*exp( -(x-b7).^2 / b8.^2 )','b1 b2 b3 b4 b5 b6 b7 b8','';
     'Lanczos3.dat','nls','lower','b1*exp(-b2*x)+b3*exp(-b4*x)+b5*exp(-b6*x)','b1 b2 b3 b4 b5 b6','[exp(-b2*x),-b1*x.*exp(-b2*x),exp(-b4*x),-b3*x.*exp(-b4*x),exp(-b6*x),-b5*x.*exp(-b6*x)]';
     'Misra1b.dat','nls','lower','b1 * (1-(1+b2*x/2).^(-2))','b1 b2','';
     'ENSO.dat','nls','average','b1+b2*cos(2*%pi*x/12)+b3*sin(2*%pi*x/12)+b5*cos(2*%pi*x/b4)+b6*sin(2*%pi*x/b4)+b8*cos(2*%pi*x/b7)+b9*sin(2*%pi*x/b7)','b1 b2 b3 b4 b5 b6 b7 b8 b9','[ones(x),cos(%pi*x/6),sin(%pi*x/6),2*%pi*x/(b4^2).*(b5*sin(2*%pi*x/b4)-b6*cos(2*%pi*x/b4)),cos(2*%pi*x/b4),sin(2*%pi*x/b4),2*%pi*x/(b7^2).*(b8*sin(2*%pi*x/b7)-b9*cos(2*%pi*x/b7)),cos(2*%pi*x/b7),sin(2*%pi*x/b7)]';
     'Gauss3.dat','nls','average','b1*exp(-b2*x)+b3*exp(-(x-b4).^2/b5^2 )+b6*exp(-(x-b7).^2/b8^2 )','b1 b2 b3 b4 b5 b6 b7 b8','';
     'Hahn1.dat','nls','average','(b1+b2*x+b3*x.^2+b4*x.^3)./(1+b5*x+b6*x.^2+b7*x.^3)','b1 b2 b3 b4 b5 b6 b7','[ones(x)./(b7*x.^3+b6*x.^2+b5*x+1),x./(b7*x.^3+b6*x.^2+b5*x+1),(x.^2)./(b7*x.^3+b6*x.^2+b5*x+1),(x.^3)./(b7*x.^3+b6*x.^2+b5*x+1),-x.*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2,-(x.^2).*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2,-(x.^3).*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2]';
     'Kirby2.dat','nls','average','(b1+b2*x+b3*x.^2)./(1+b4*x+b5*x.^2)','b1 b2 b3 b4 b5','[ones(x)./(b5*x.^2+b4*x+1.),x./(b5*x.^2+b4*x+1.),(x.^2)./(b5*x.^2+b4*x+1.),-x.*(b3*x.^2+b2*x+b1)./(b5*x.^2+b4*x+1.).^2,-(x.^2).*(b3*x.^2+b2*x+b1)./(b5*x.^2+b4*x+1.).^2]';
     'Lanczos1.dat','nls','average','b1*exp(-b2*x) + b3*exp(-b4*x) + b5*exp(-b6*x)','b1 b2 b3 b4 b5 b6','[exp(-b2*x),-b1*x.*exp(-b2*x),exp(-b4*x),-b3*x.*exp(-b4*x),exp(-b6*x),-b5*x.*exp(-b6*x)]';
     'Lanczos2.dat','nls','average','b1*exp(-b2*x) + b3*exp(-b4*x) + b5*exp(-b6*x)','b1 b2 b3 b4 b5 b6','[exp(-b2*x),-b1*x.*exp(-b2*x),exp(-b4*x),-b3*x.*exp(-b4*x),exp(-b6*x),-b5*x.*exp(-b6*x)]';
     'MGH17.dat','nls','average','b1+b2*exp(-x*b4)+b3*exp(-x*b5)','b1 b2 b3 b4 b5','[ones(x),exp(-b4*x),exp(-b5*x),-b2*x.*exp(-b4*x),-b3*x.*exp(-b5*x)]';
     'Misra1c.dat','nls','average','b1*(1-(1+2*b2*x).^(-.5))','b1 b2','';
     'Misra1d.dat','nls','average','b1*b2*x.*((1+b2*x).^(-1))','b1 b2','';
//     'Nelson.dat','nls','average','exp(b1-b2*x1.*exp(-b3*x2))','b1 b2',''; // file bug on nistdata_read for this data set (fails to read x1 and x2...)
//     'Roszman1.dat','nls','average','b1-b2*x-atan(b3./(x-b4))/%pi','b1 b2 b3 b4','[ones(x),-x,-ones(x)./(%pi*(x-b4).*((b3./(x-b4)).^2+1)),-b3./(%pi*((x-b4).^2).*((b3./(x-b4)).^2+1))]';
     'Roszman1.dat','nls','average','b1-b2*x-atan(b3./(x-b4))/%pi','b1 b2 b3 b4','';
     'Bennett5.dat','nls','higher','b1*(b2+x).^(-1/b3)','b1 b2 b3','[ones(x)./(x+b2).^(1/b3),-b1*(x+b2).^(-1/b3-1)/b3,b1*log(x+b2)./(b3^2*(x+b2).^(1/b3))]';
     'BoxBOD.dat','nls','higher','b1*(1-exp(-b2*x))','b1 b2','[1.0-exp(-b2*x),b1*x.*exp(-b2*x)]';
     'Eckerle4.dat','nls','higher','(b1/b2)*exp(-0.5*((x-b3)/b2).^2)','b1 b2 b3','';
     'MGH09.dat','nls','higher','b1*(x.^2+x*b2)./(x.^2+x*b3+b4)','b1 b2 b3 b4','[(x.^2+b2*x)./(x.^2+b3*x+b4),b1*x./(x.^2+b3*x+b4),-b1*x.*(x.^2+b2*x)./(x.^2+b3*x+b4).^2,-b1*(x.^2+b2*x)./(x.^2+b3*x+b4).^2]';
     'MGH10.dat','nls','higher','b1*exp(b2./(x+b3))','b1 b2 b3','[exp(b2./(x+b3)),b1*exp(b2./(x+b3))./(x+b3),-b1*b2*exp(b2./(x+b3))./(x+b3).^2]';
     'Rat42.dat','nls','higher','b1./(1+exp(b2-b3*x))','b1 b2 b3','[ones(x)./(exp(b2-b3*x)+1),-b1*exp(b2-b3*x)./(exp(b2-b3*x)+1).^2,b1*x.*exp(b2-b3*x)./(exp(b2-b3*x)+1).^2]';
     'Rat43.dat','nls','higher','b1./((1+exp(b2-b3*x)).^(1/b4))','b1 b2 b3 b4','';
     'Thurber.dat','nls','higher','(b1+b2*x+b3*x.^2+b4*x.^3)./(1+b5*x+b6*x.^2+b7*x.^3)','b1 b2 b3 b4 b5 b6 b7','[ones(x)./(b7*x.^3+b6*x.^2+b5*x+1),x./(b7*x.^3+b6*x.^2+b5*x+1),(x.^2)./(b7*x.^3+b6*x.^2+b5*x+1),(x.^3)./(b7*x.^3+b6*x.^2+b5*x+1),-x.*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2,-(x.^2).*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2,-(x.^3).*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2]'
     ];

function [Passed]=nlinregr_report_tests(phat,data)
    tol=1d-4;
    Passed=%t;
    try assert_checkalmostequal(phat,data.parameter,tol) 
    catch 
        printf('\tFailed');
        Passed=%f; 
//        disp([phat data.parameter])
    end
    if Passed then 
        printf('\t  Ok'); 
    end
endfunction

function res=lsqrsolve_res(p,m)
    res = data.y-curve(p,[data.x data.y]);
endfunction

function dres_dp=lsqrsolve_dres_dp(p,m)
    dres_dp = -dcurve(p,[data.x data.y]);
endfunction


// nls datasets
path=nistdataset_getpath();
SumPassed=[0 0 0]; SumFailed=[0 0 0]; 
printf('\n NIST dataset  (difficulty)\tStart estimate: Status=Ok|Failed|Crash\n');
printf('----------------------------------------------------------------------------------------\n');
for i=1:size(sets,'r')
    filename = fullfile(path,"datasets",sets(i,2),sets(i,3),sets(i,1));
    data = nistdataset_read(filename);
    
    printf('%15s (%7s)   ',sets(i,1),sets(i,3));
    funDef=sets(i,4); dfunDef=sets(i,6); pDef=sets(i,5); dfunDef='';
    
   // Convert regression function into the curve function
   formula=nlinregr_formula(funDef,pDef,'p(%i)');
   formula=nlinregr_formula(formula,sprintf('%s ','x y'),'data(:,%i)');            
   deff('[yhat]=curve(p,data)','yhat='+formula);

   if  ~isempty(dfunDef) then
      dformula=nlinregr_formula(dfunDef,pDef,'p(%i)');
      dformula=nlinregr_formula(dformula,sprintf('%s ','x y'),'data(:,%i)');            
      deff('[dyhat_dp]=dcurve(p,data)','dyhat_dp='+dformula);
   end
   
    printf('Start1: ');    Chrashed=%f;
    diag_=ones(data.start1);//./data.start1;
    if isempty(dfunDef) then
        [phat,yhat,stat]=lsqrsolve(data.start1,lsqrsolve_res,length(data.y));
    else
        [phat,yhat,stat]=lsqrsolve(data.start1,lsqrsolve_res,length(data.y),lsqrsolve_dres_dp);
    end
    Passed=nlinregr_report_tests(phat,data); 
    if Passed & ~Chrashed then SumPassed(1)=SumPassed(1)+1; else SumFailed(1)=SumFailed(1)+1; end
    //disp([data.start1 phat data.parameter]); pause;
    
    printf('\tStart2: ');    Chrashed=%f;
    if isempty(dfunDef) then
        [phat,yhat,stat]=lsqrsolve(data.start2,lsqrsolve_res,length(data.y));
    else
        [phat,yhat,stat]=lsqrsolve(data.start2,lsqrsolve_res,length(data.y),lsqrsolve_dres_dp);
    end
    Passed=nlinregr_report_tests(phat,data); 
    if Passed & ~Chrashed then SumPassed(2)=SumPassed(2)+1; else SumFailed(2)=SumFailed(2)+1; end

    printf('\tSolution:');    Chrashed=%f;
    if isempty(dfunDef) then
        [phat,yhat,stat]=lsqrsolve(data.parameter,lsqrsolve_res,length(data.y));
    else
        [phat,yhat,stat]=lsqrsolve(data.parameter,lsqrsolve_res,length(data.y),lsqrsolve_dres_dp);
    end
    Passed=nlinregr_report_tests(phat,data);
    if Passed & ~Chrashed then SumPassed(3)=SumPassed(3)+1; else SumFailed(3)=SumFailed(3)+1; end

    printf('\n');
end
printf('----------------------------------------------------------------------------------------\n');
printf("Total score (passed|failed):\tStart1: (%i|%i)\tStart2: (%i|%i)\tSolution:\t(%i|%i)\n",SumPassed(1),SumFailed(1),SumPassed(2),SumFailed(2),SumPassed(3),SumFailed(3));
printf('----------------------------------------------------------------------------------------\n');













