
sets=[
     'Lanczos3.dat','nls','lower','b1*exp(-b2*x)+b3*exp(-b4*x)+b5*exp(-b6*x)','b1 b2 b3 b4 b5 b6','[exp(-b2*x),-b1*x.*exp(-b2*x),exp(-b4*x),-b3*x.*exp(-b4*x),exp(-b6*x),-b5*x.*exp(-b6*x)]';
     'ENSO.dat','nls','average','b1+b2*cos(2*%pi*x/12)+b3*sin(2*%pi*x/12)+b5*cos(2*%pi*x/b4)+b6*sin(2*%pi*x/b4)+b8*cos(2*%pi*x/b7)+b9*sin(2*%pi*x/b7)','b1 b2 b3 b4 b5 b6 b7 b8 b9','[ones(x),cos(%pi*x/6),sin(%pi*x/6),2*%pi*x/(b4^2).*(b5*sin(2*%pi*x/b4)-b6*cos(2*%pi*x/b4)),cos(2*%pi*x/b4),sin(2*%pi*x/b4),2*%pi*x/(b7^2).*(b8*sin(2*%pi*x/b7)-b9*cos(2*%pi*x/b7)),cos(2*%pi*x/b7),sin(2*%pi*x/b7)]';
     'Hahn1.dat','nls','average','(b1+b2*x+b3*x.^2+b4*x.^3)./(1+b5*x+b6*x.^2+b7*x.^3)','b1 b2 b3 b4 b5 b6 b7','[ones(x)./(b7*x.^3+b6*x.^2+b5*x+1),x./(b7*x.^3+b6*x.^2+b5*x+1),(x.^2)./(b7*x.^3+b6*x.^2+b5*x+1),(x.^3)./(b7*x.^3+b6*x.^2+b5*x+1),-x.*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2,-(x.^2).*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2,-(x.^3).*(b4*x.^3+b3*x.^2+b2*x+b1)./(b7*x.^3+b6*x.^2+b5*x+1).^2]';
     'Kirby2.dat','nls','average','(b1+b2*x+b3*x.^2)./(1+b4*x+b5*x.^2)','b1 b2 b3 b4 b5','[ones(x)./(b5*x.^2+b4*x+1.),x./(b5*x.^2+b4*x+1.),(x.^2)./(b5*x.^2+b4*x+1.),-x.*(b3*x.^2+b2*x+b1)./(b5*x.^2+b4*x+1.).^2,-(x.^2).*(b3*x.^2+b2*x+b1)./(b5*x.^2+b4*x+1.).^2]';
     'Lanczos1.dat','nls','average','b1*exp(-b2*x) + b3*exp(-b4*x) + b5*exp(-b6*x)','b1 b2 b3 b4 b5 b6','[exp(-b2*x),-b1*x.*exp(-b2*x),exp(-b4*x),-b3*x.*exp(-b4*x),exp(-b6*x),-b5*x.*exp(-b6*x)]';
     'Lanczos2.dat','nls','average','b1*exp(-b2*x) + b3*exp(-b4*x) + b5*exp(-b6*x)','b1 b2 b3 b4 b5 b6','[exp(-b2*x),-b1*x.*exp(-b2*x),exp(-b4*x),-b3*x.*exp(-b4*x),exp(-b6*x),-b5*x.*exp(-b6*x)]';
     'MGH17.dat','nls','average','b1+b2*exp(-x*b4)+b3*exp(-x*b5)','b1 b2 b3 b4 b5','[ones(x),exp(-b4*x),exp(-b5*x),-b2*x.*exp(-b4*x),-b3*x.*exp(-b5*x)]';
    'MGH09.dat','nls','higher','b1*(x.^2+x*b2)./(x.^2+x*b3+b4)','b1 b2 b3 b4','';
     'MGH10.dat','nls','higher','b1*exp(b2./(x+b3))','b1 b2 b3','[exp(b2./(x+b3)),b1*exp(b2./(x+b3))./(x+b3),-b1*b2*exp(b2./(x+b3))./(x+b3).^2]';
     ];

i=7;
path=nistdataset_getpath();
filename = fullfile(path,"datasets",sets(i,2),sets(i,3),sets(i,1));
data = nistdataset_read(filename);

funDef=sets(i,4); dfunDef=sets(i,6); pDef=sets(i,5);

span=0.1;
pEst=data.start1; pLo=pEst*(1-span); pUp=pEst*(1+span);
[pEst,yhat,stat]=nlinregr([data.x data.y],'x y',funDef,dfunDef,pDef,'y',1,pEst,pLo,pUp);
ActiveLo=find(pEst==pLo);
ActiveUp=find(pEst==pUp);
warning('off');

itt=1; 
while ~isempty(ActiveLo) | ~isempty(ActiveUp)
    printf('itt: %i: ActiveLo=%i | ActiveUp=%i\n',itt,length(ActiveLo),length(ActiveUp));
    pLo=pEst*(1-span); pUp=pEst*(1+span);

    [pEst,yhat,stat]=nlinregr([data.x data.y],'x y',funDef,dfunDef,pDef,'y',1,pEst,pLo,pUp);
    ActiveLo=find(pEst==pLo);
    ActiveUp=find(pEst==pUp);
    itt=itt+1;
end
printf('    pLo    \t    pEst    \t   pUp    \t    True\n');
printf('%f\t%f\t%f\t%f\n',pLo,pEst,pUp,data.parameter);


