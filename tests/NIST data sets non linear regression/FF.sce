
// Data fitting problem
// 1 build the data
a=34;
b=12;
c=14;

function y=FF(x)
  y=a*(x-b)+c*x.*x
endfunction
X=(0:.1:3)';
Y=FF(X)+100*(rand()-.5);

//solve
function e=f1(abc, m,XYin)
  a=abc(1);
  b=abc(2);
  c=abc(3);
  e=XYin(2)-(a*(XYin(1)-b)+c*XYin(1).*XYin(1));
endfunction

function e=df1_dabc(abc, m,Xin,Yin)
  a=abc(1);
  b=abc(2);
  c=abc(3);
  e=Yin-(a*(Xin-b)+c*Xin.*Xin);
  e=[-(Xin-b),a*ones(Xin),-Xin.^2];
endfunction

[abc,v]=lsqrsolve([10;10;10],list(f1,list(X,Y)),size(X,1))

//[abc,v]=lsqrsolve([10;10;10],list(f1,X,Y),size(X,1),list(df1_dabc,X,Y))
abc
norm(v)
