deff('yhat=nlinmod(p,x,A)',['yhat=A*p(1)+p(2)*exp(p(3)*x);']); // define regression model to be fitted
deff('dydp=dnlinmod(p,x,A)',['dydp=[A*ones(x),exp(p(3)*x),p(2)*x.*exp(p(3)*x)];']); // define d(yhat)/dp
x=-[1:100]'/10; phat=[2;10;0.5];               // generate some data points
A=2;
y=nlinmod(phat,x,A)+grand(100,1,'nor',0,1)/2;    // with with random noice added.
p0=ones(3,1); // initial estimate for the regression parameters.
// Solve nonlinear regression problem with output every 4'th iteration and nameing of model parmameters.
[p,stat]=nlinlsq(list(nlinmod,x,A),list(dnlinmod,x,A),y,[],p0,[],[],list(4,'A B C'));
//[p,stat]=nlinlsq(list(nlinmod,x,A),[],y,[],p0,[],[],list(4,'A B C'));
