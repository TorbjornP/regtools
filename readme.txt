﻿Title : regtools
Type : Scilab toolbox
Version : 0.42
Date : 06-Jan-2016
Author : Torbjørn Pettersen <top@tpett.com>
Description : A toolbox for linear and non linear regression
Licence : See license.txt

Installation and usage:
 1) First time installation:
   - copy the directory regtools to the SCI\contrib
   - from the Scilab Console, run the following commands:
       exec SCI\contrib\regtools\builder.sce // only for non Windows installations
       exec SCI\contrib\regtools\loader.sce

 2) Later usage:
   - load toolbox regtools from the Toolboxes menu in the Scilab Console.

 3) Help and documentation
   - see section "Regression tools (regtools)" in the Scilab help browser
   - see section "Regression tools (regtools)" in the Scilab demonstrations menu
