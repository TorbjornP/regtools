mode(1)
//
// Demo of ff2n.sci
//

dFF2 = ff2n(3) // 2^3 full factorial design
dFF2alt = ff2n(3,%f) // 2^3 full factorial design with [1 2] as level indicators
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
