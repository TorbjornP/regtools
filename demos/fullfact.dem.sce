mode(1)
//
// Demo of fullfact.sci
//

Design23 = fullfact([2 2 2]) // 2^3 full factorial design
Design32 = fullfact([3 3]) // 3^2 full factorial design
halt()   // Press return to continue
 
//========= E N D === O F === D E M O =========//
